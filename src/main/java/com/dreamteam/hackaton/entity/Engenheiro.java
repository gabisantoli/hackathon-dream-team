package com.dreamteam.hackaton.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Engenheiro {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	private String nome;
	private String email;
	private String telefone;
	private String racf;
	@ManyToOne
	@JoinColumn(name = "squad_id")
	private Squad squad;
	@OneToOne
    @JoinColumn(name = "tecnologia_id")
	private Tecnologia tecnologia;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getRacf() {
		return racf;
	}
	public void setRacf(String racf) {
		this.racf = racf;
	}
	public Squad getSquad() {
		return squad;
	}
	public void setSquad(Squad squad) {
		this.squad = squad;
	}
	public Tecnologia getTecnologia() {
		return tecnologia;
	}
	public void setTecnologia(Tecnologia tecnologia) {
		this.tecnologia = tecnologia;
	}
}
