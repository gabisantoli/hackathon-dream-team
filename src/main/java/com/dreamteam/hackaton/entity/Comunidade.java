package com.dreamteam.hackaton.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Comunidade {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	private String nome;
	private String gerente;
	@OneToMany(mappedBy = "comunidade")
	private List<Rt> rts;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getGerente() {
		return gerente;
	}
	public void setGerente(String gerente) {
		this.gerente = gerente;
	}
	public List<Rt> getRts() {
		return rts;
	}
	public void setRts(List<Rt> rts) {
		this.rts = rts;
	}
}
