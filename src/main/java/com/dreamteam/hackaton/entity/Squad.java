package com.dreamteam.hackaton.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Squad {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	private String nome;
	@ManyToOne
    @JoinColumn(name = "rt_id")
    private Rt rt;
	@OneToOne
    @JoinColumn(name = "cliente_id")
	private Cliente cliente;
	@OneToMany(mappedBy = "squad")
    private List<Engenheiro> engenheiros;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Rt getRt() {
		return rt;
	}
	public void setRt(Rt rt) {
		this.rt = rt;
	}
	public List<Engenheiro> getEngenheiros() {
		return engenheiros;
	}
	public void setEngenheiros(List<Engenheiro> engenheiros) {
		this.engenheiros = engenheiros;
	}
}
