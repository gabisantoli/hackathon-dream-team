package com.dreamteam.hackaton.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Rt {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	private String nome;
	private String teamLead;
	private String teachLead;
	private String gerente;
	private String coordenador;
	@ManyToOne
    @JoinColumn(name = "comunidade_id")
    private Comunidade comunidade;
	@OneToMany(mappedBy = "rt")
	private List<Squad> squads;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTeamLead() {
		return teamLead;
	}
	public void setTeamLead(String teamLead) {
		this.teamLead = teamLead;
	}
	public String getTeachLead() {
		return teachLead;
	}
	public void setTeachLead(String teachLead) {
		this.teachLead = teachLead;
	}
	public String getGerente() {
		return gerente;
	}
	public void setGerente(String gerente) {
		this.gerente = gerente;
	}
	public String getCoordenador() {
		return coordenador;
	}
	public void setCoordenador(String coordenador) {
		this.coordenador = coordenador;
	}
	public List<Squad> getSquads() {
		return squads;
	}
	public void setSquads(List<Squad> squads) {
		this.squads = squads;
	}
	public Comunidade getComunidade() {
		return comunidade;
	}
	public void setComunidade(Comunidade comunidade) {
		this.comunidade = comunidade;
	}
}
