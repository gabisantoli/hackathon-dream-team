package com.dreamteam.hackaton.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.dreamteam.hackaton.entity.Rt;

@RepositoryRestResource(collectionResourceRel = "rts", path = "rts")
public interface RtRepository extends PagingAndSortingRepository<Rt, Long> {
    List<Rt> findByNome(@Param("nome") String nome);
}