package com.dreamteam.hackaton.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.dreamteam.hackaton.entity.Usuario;

@RepositoryRestResource(collectionResourceRel = "usuarios", path = "usuarios")
public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Long> {
    List<Usuario> findByNome(@Param("nome") String nome);
    Usuario findByLoginAndSenha(@Param("login") String login,@Param("senha") String senha);
}