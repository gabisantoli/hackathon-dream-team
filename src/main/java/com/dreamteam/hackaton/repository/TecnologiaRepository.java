package com.dreamteam.hackaton.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.dreamteam.hackaton.entity.Tecnologia;

@RepositoryRestResource(collectionResourceRel = "tecnologias", path = "tecnologias")
public interface TecnologiaRepository extends PagingAndSortingRepository<Tecnologia, Long> {
    List<Tecnologia> findByNome(@Param("nome") String nome);
}