package com.dreamteam.hackaton.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.dreamteam.hackaton.entity.Engenheiro;

@RepositoryRestResource(collectionResourceRel = "engenheiros", path = "engenheiros")
public interface EngenheirosRepository extends PagingAndSortingRepository<Engenheiro, Long> {
    List<Engenheiro> findByNome(@Param("nome") String nome);
}