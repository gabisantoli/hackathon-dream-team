package com.dreamteam.hackaton.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.dreamteam.hackaton.entity.Squad;

@RepositoryRestResource(collectionResourceRel = "squads", path = "squads")
public interface SquadRepository extends PagingAndSortingRepository<Squad, Long> {
    List<Squad> findByNome(@Param("nome") String nome);
}