package com.dreamteam.hackaton.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.dreamteam.hackaton.entity.Comunidade;

@RepositoryRestResource(collectionResourceRel = "comunidades", path = "comunidades")
public interface ComunidadeRepository extends PagingAndSortingRepository<Comunidade, Long> {
    List<Comunidade> findByNome(@Param("nome") String nome);
}