package com.dreamteam.hackaton.util;

import java.util.ArrayList;
import java.util.List;

import com.dreamteam.hackaton.entity.Cliente;
import com.dreamteam.hackaton.entity.Comentario;
import com.dreamteam.hackaton.entity.Comunidade;
import com.dreamteam.hackaton.entity.Engenheiro;
import com.dreamteam.hackaton.entity.Post;
import com.dreamteam.hackaton.entity.Rt;
import com.dreamteam.hackaton.entity.Squad;
import com.dreamteam.hackaton.entity.Tecnologia;
import com.dreamteam.hackaton.entity.Usuario;

public class Mocks {
	
	public static Squad squadMock() {
		Comunidade comunidade = new Comunidade();
		comunidade.setId(0);
		comunidade.setNome("Dream Team");
		
		Rt rt = new Rt();
		rt.setId(0);
		rt.setNome("Canais");
		rt.setComunidade(comunidade);
		
		Cliente cliente = new Cliente();
		cliente.setId(0);
		cliente.setNome("Itau");
		
		Squad squad = new Squad();
		squad.setId(0);
		squad.setCliente(cliente);
		squad.setRt(rt);
		squad.setNome("Ativos 2");
		   		
		return squad;
	}
	
	public static Tecnologia tecnologiaMock() {
		final Tecnologia tec = new Tecnologia();
		tec.setId(0);
		tec.setLtf("Sassine");
		tec.setNome("Full Stack");
		return tec;
	}
	
	public static Engenheiro engenheiroMock() {
		final Engenheiro engenheiro = new Engenheiro();
		engenheiro.setNome("Gabriela Oliveira");
		engenheiro.setEmail("gsantool@everis.com");
		engenheiro.setRacf("gabrstl");
		engenheiro.setSquad(Mocks.squadMock());
		engenheiro.setTelefone("11999999999");
		engenheiro.setTecnologia(Mocks.tecnologiaMock());
		return engenheiro;
	}
	
	public static Post postMock() {
		Usuario usuario = new Usuario();
		usuario.setId(0);
		usuario.setLogin("usuario@padrao.com");
		usuario.setNome("Usuario Padrao");
		usuario.setSenha("123456");
		
		Comentario comentario =  new Comentario();
		comentario.setId(0);
		comentario.setConteudo("Parabens!");
		comentario.setUsuario(usuario);
		List<Comentario> comentarios = new ArrayList<Comentario>();
		comentarios.add(comentario);
		
		Post post = new Post();
		post.setId(0);
		post.setTitulo("Hackathon Dream Team");
		post.setSubTitulo("Se voce esta vendo isso é pq deu certo");
		post.setImagem("https://images.freeimages.com/images/small-previews/e2f/hide-and-seek-1436876.jpg");
		post.setConteudo("Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.");
		post.setUsuario(usuario);
		post.setComentarios(comentarios);	
		
		return post;
	}

}
