package com.dreamteam.hackaton.response;

public class OrganizacaoViewReponse {
	
	private String comunidade;
	private String rt;
	private String squad;
	private String cliente;
	
	public String getComunidade() {
		return comunidade;
	}
	public void setComunidade(String comunidade) {
		this.comunidade = comunidade;
	}
	public String getRt() {
		return rt;
	}
	public void setRt(String rt) {
		this.rt = rt;
	}
	public String getSquad() {
		return squad;
	}
	public void setSquad(String squad) {
		this.squad = squad;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

}
