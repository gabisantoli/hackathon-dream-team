package com.dreamteam.hackaton.response;

import java.util.List;

public class PostsViewResponse {
	
	private long id;
	private String usuarioNome;
	private String imagem;
	private String titulo;
	private String subTitulo;
	private String conteudo;
	private List<ComentarioResponse> comentarios;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsuarioNome() {
		return usuarioNome;
	}
	public void setUsuarioNome(String usuarioNome) {
		this.usuarioNome = usuarioNome;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getSubTitulo() {
		return subTitulo;
	}
	public void setSubTitulo(String subTitulo) {
		this.subTitulo = subTitulo;
	}
	public String getConteudo() {
		return conteudo;
	}
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	public List<ComentarioResponse> getComentarios() {
		return comentarios;
	}
	public void setComentarios(List<ComentarioResponse> comentarios) {
		this.comentarios = comentarios;
	}	

}
