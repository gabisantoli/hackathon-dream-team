package com.dreamteam.hackaton.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dreamteam.hackaton.entity.Engenheiro;
import com.dreamteam.hackaton.repository.EngenheirosRepository;
import com.dreamteam.hackaton.response.EngenheirosViewReponse;
import com.dreamteam.hackaton.util.Mocks;

@RepositoryRestController
@RequestMapping("/engenheiro")
public class EngenheirosViewController {
	
	private final EngenheirosRepository engenheirosRepository;

    @Autowired
    public EngenheirosViewController(EngenheirosRepository repo) { 
    	engenheirosRepository = repo;
    }

	@RequestMapping(method=RequestMethod.GET, value = "/view") 
    public @ResponseBody ResponseEntity<List<EngenheirosViewReponse>> engenheirosView() {
        
    	List<Engenheiro> engenheiros = (List<Engenheiro>) engenheirosRepository.findAll();
    	
    	//mock caso não tenha cadastrado
    	if(CollectionUtils.isEmpty(engenheiros)) {
    		engenheiros = new ArrayList<>();
    		engenheiros.add(Mocks.engenheiroMock());
    	}
    	
    	final List<EngenheirosViewReponse> response = new ArrayList<>();
    	
    	for(Engenheiro engenheiro: engenheiros) {
    		final EngenheirosViewReponse engenheirosViewReponse = new EngenheirosViewReponse();
    		engenheirosViewReponse.setId(0);
    		engenheirosViewReponse.setNome(engenheiro.getNome());
    		engenheirosViewReponse.setEmail(engenheiro.getEmail());
    		engenheirosViewReponse.setTelefone(engenheiro.getTelefone());
    		engenheirosViewReponse.setRacf(engenheiro.getRacf());
    		engenheirosViewReponse.setCliente(engenheiro.getSquad().getCliente().getNome());
    		engenheirosViewReponse.setSquad(engenheiro.getSquad().getNome());
    		engenheirosViewReponse.setTecnologia(engenheiro.getTecnologia().getNome());
    		response.add(engenheirosViewReponse);
    	}
    	
        return ResponseEntity.ok(response); 
    }
}
