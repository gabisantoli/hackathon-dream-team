package com.dreamteam.hackaton.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dreamteam.hackaton.entity.Usuario;
import com.dreamteam.hackaton.repository.UsuarioRepository;
import com.dreamteam.hackaton.request.LoginRequest;

@RepositoryRestController
@RequestMapping("/usuario")
public class UsuarioController {

    private final UsuarioRepository repository;

    @Autowired
    public UsuarioController(UsuarioRepository repo) { 
        repository = repo;
    }

    @RequestMapping(method=RequestMethod.POST, value = "/login") 
    public @ResponseBody ResponseEntity<Usuario> login(@RequestBody LoginRequest login) {
        
    	Usuario usuario = repository.findByLoginAndSenha(login.getEmail(),login.getSenha());
    	
    	//mock caso não tenha usuario cadastrado
    	if(usuario == null) {
    		usuario = new Usuario();
    		usuario.setId(0);
    		usuario.setLogin("usuario@padrao.com");
    		usuario.setNome("Usuario Padrao");
    		usuario.setSenha("123456");
    	}
    	
        return ResponseEntity.ok(usuario); 
    }

}
