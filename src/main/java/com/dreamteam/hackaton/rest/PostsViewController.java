package com.dreamteam.hackaton.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dreamteam.hackaton.entity.Comentario;
import com.dreamteam.hackaton.entity.Post;
import com.dreamteam.hackaton.repository.PostRepository;
import com.dreamteam.hackaton.response.ComentarioResponse;
import com.dreamteam.hackaton.response.PostsViewResponse;
import com.dreamteam.hackaton.util.Mocks;


@RepositoryRestController
@RequestMapping("/post")
public class PostsViewController {

    private final PostRepository postRepository;

    @Autowired
    public PostsViewController(PostRepository post) { 
    	postRepository = post;
    }

    @RequestMapping(method=RequestMethod.GET, value = "/view")  
    public @ResponseBody ResponseEntity<List<PostsViewResponse>> postsView() {
        
    	List<Post> posts = (List<Post>) postRepository.findAll();
    	
    	//mock caso não tenha cadastrado
    	if(CollectionUtils.isEmpty(posts)) {
    		posts = new ArrayList<>();
    		posts.add(Mocks.postMock());
    	}
    	
    	final List<PostsViewResponse> response = new ArrayList<>();
    	
    	for(Post post: posts) {
    		final PostsViewResponse postsViewResponse = new PostsViewResponse();
    		postsViewResponse.setId(post.getId());
    		postsViewResponse.setTitulo(post.getTitulo());
    		postsViewResponse.setSubTitulo(post.getSubTitulo());
    		postsViewResponse.setUsuarioNome(post.getUsuario().getNome());
    		postsViewResponse.setConteudo(post.getConteudo());
    		postsViewResponse.setImagem(post.getImagem());
    		final List<ComentarioResponse> comentariosResponse = new ArrayList<>();
    		for(Comentario comentario: post.getComentarios()) {
    			final ComentarioResponse comentarioRes = new ComentarioResponse();
    			comentarioRes.setId(comentario.getId());
    			comentarioRes.setUsuarioNome(comentario.getUsuario().getNome());
    			comentarioRes.setConteudo(comentario.getConteudo());    
    			comentariosResponse.add(comentarioRes);
    		}
    		postsViewResponse.setComentarios(comentariosResponse);
    		response.add(postsViewResponse);
    	}
    	
        return ResponseEntity.ok(response); 
    }

}