package com.dreamteam.hackaton.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dreamteam.hackaton.entity.Squad;
import com.dreamteam.hackaton.repository.SquadRepository;
import com.dreamteam.hackaton.response.OrganizacaoViewReponse;
import com.dreamteam.hackaton.util.Mocks;

@RepositoryRestController
@RequestMapping("/organizacao")
public class OrganizacaoViewController {
	
	private final SquadRepository squadRepository;

    @Autowired
    public OrganizacaoViewController(SquadRepository repo) { 
    	squadRepository = repo;
    }

	@RequestMapping(method=RequestMethod.GET, value = "/view")  
    public @ResponseBody ResponseEntity<List<OrganizacaoViewReponse>> postsView() {
        
    	List<Squad> squads = (List<Squad>) squadRepository.findAll();
    	
    	//mock caso não tenha cadastrado
    	if(CollectionUtils.isEmpty(squads)) {
    		squads = new ArrayList<>();
    		squads.add(Mocks.squadMock());
    	}
    	
    	final List<OrganizacaoViewReponse> response = new ArrayList<>();
    	
    	for(Squad squad: squads) {
    		final OrganizacaoViewReponse organizacaoViewResponse = new OrganizacaoViewReponse();
    		organizacaoViewResponse.setCliente(squad.getCliente().getNome());
    		organizacaoViewResponse.setComunidade(squad.getRt().getComunidade().getNome());
    		organizacaoViewResponse.setRt(squad.getRt().getNome());
    		organizacaoViewResponse.setSquad(squad.getNome());  
    		response.add(organizacaoViewResponse);
    	}
    	
        return ResponseEntity.ok(response); 
    }
}
